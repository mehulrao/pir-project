import RPi.GPIO as GPIO
import time
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.setup(4, GPIO.IN)
GPIO.setup(17, GPIO.OUT)
GPIO.setup(15, GPIO.OUT)
GPIO.setup(5, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(22, GPIO.IN, pull_up_down=GPIO.PUD_UP)
import httplib
import urllib
while True:
    GPIO.output(17, GPIO.LOW)
    GPIO.output(15, GPIO.LOW)
    w = GPIO.input(5)
    if w == False:
        time.sleep(0.1)
        print "ARMED"
        while True:
            l = GPIO.input(22)
            if l == False:
                  print "HOME"
                  break
                  time.sleep(1)
            i=GPIO.input(4)
            if i==0:                 #When output from motion sensor is LOW
                print "No Motion",i
                time.sleep(0.2)
                GPIO.output(17, GPIO.HIGH)
                GPIO.output(15, GPIO.LOW)
            elif i==1:               #When output from motion sensor is HIGH
                 print "Motion Detected",i
                  GPIO.output(15, GPIO.HIGH)
                  GPIO.output(17, GPIO.LOW)
                  conn = httplib.HTTPSConnection("api.pushover.net:443")
                  conn.request("POST", "/1/messages.json",
                  urllib.urlencode({
                     "token": "auqm7ob5ak3cxhp8gx2u3d2f21ipc3",
                      "user": "ufdmy1vnvsjt7uh1thjksfs4qt2wzg",
                      "message": "Motion Detected!!",
                  }), { "Content-type": "application/x-www-form-urlencoded" })
                  conn.getresponse()